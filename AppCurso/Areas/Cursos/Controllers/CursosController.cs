﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppCurso.Areas.Cursos.Models;
using AppCurso.Data;
using AppCurso.Library;
using AppCurso.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace AppCurso.Areas.Cursos.Controllers
{
    [Area("Cursos")]
    [Authorize(Roles = "Admin")]
    public class CursosController : Controller
    {
        private LCategorias _lCategoria;
        private LCursos _curso;
        private SignInManager<IdentityUser> _signInManager;
        private static DataPaginador<TCursos> models;
        private static IdentityError identityError;
        private ApplicationDbContext _context;
        
        public CursosController(ApplicationDbContext context,SignInManager<IdentityUser> signInManager,IWebHostEnvironment environment)
        {
            _signInManager = signInManager;
            _lCategoria = new LCategorias(context);
            _curso = new LCursos(context, environment);
        }
        [Authorize(Roles ="Admin")]
        public IActionResult Cursos(int id,string search,int registros)
        {
            if (_signInManager.IsSignedIn(User))
            {
                Object[] objects = new Object[3];
                var data = _curso.getTCursos(search);
                if (data.Count>0)
                {
                    var url = Request.Scheme + "://" + Request.Host.Value;
                    objects = new LPaginador<TCursos>().paginador(data, id, registros, "Cursos", "Cursos", "Cursos", url);
                }
                else
                {
                    objects[0]= "NO hay datos que mostrar.";
                    objects[1] = "NO hay datos que mostrar.";
                    objects[2] = new List<TCursos>();
                }
                models = new DataPaginador<TCursos>
                {
                    List= (List<TCursos>)objects[2],
                    Pagi_info=(string)objects[0],
                    Pagi_navegacion=(string)objects[1],
                    Categorias = _lCategoria.getTCategoria(),
                    Input = new TCursos(),

                };
                if (identityError!=null)
                {
                    models.Pagi_info = identityError.Description;
                    identityError = null;
                }
                return View(models);
            }
            else
            {
                return Redirect("/Home/Index");
            }
        }
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public String GetCurso(DataPaginador<TCursos> model)
        {
            if (model.Input.Curso!=null&&model.Input.Informacion!=null && model.Input.CategoriaID!=0)
            {
                if (model.Input.Horas==0)
                {
                    return "Ingrese las horas del curso";
                }
                else
                {
                    if (model.Input.Costo.Equals(0.00M))
                    {
                        return "Ingrese el costo del curso.";
                    }
                    else
                    {
                        var data = _curso.RegistrarCursoAsync(model);
                        return JsonConvert.SerializeObject(data.Result);
                    }
                }
            }
            else
            {
                return "Llene los campos requeridos.";
            }
        }
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult updateEstado(int id)
        {
            identityError = _curso.updateEstado(id);
            return Redirect("/Cursos/Cursos?area=Cursos");
        }
        [HttpPost]
        public String EliminarCurso(int CursoId)
        {
            identityError = _curso.DeleteCurso(CursoId);
            return JsonConvert.SerializeObject(identityError);
        }
    }
} 