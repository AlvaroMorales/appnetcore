﻿using AppCurso.Areas.Categorias.Models;
using AppCurso.Areas.Inscripciones.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace AppCurso.Areas.Cursos.Models
{
    public class TCursos
    {
        [Key]
        public int CursoId { get; set; }
        [Required(ErrorMessage = "Debe ingresar el nombre del curso.")]
        public string Curso { get; set; }
        [Required(ErrorMessage = "Debe ingresar una descripción.")]
        public string Informacion { get; set; }
        [Required(ErrorMessage = "Ingrese las horas del curso.")]
        public byte Horas { get; set; }
        [Required(ErrorMessage = "Ingrese el costo del curso.")]
        [RegularExpression(@"^[0-9]+([.][0-9]+)?$", ErrorMessage = "El costo no es correcto.")]
        public decimal Costo { get; set; }
        public Boolean Estado { get; set; }
        public byte[] Imagen { get; set; }
        public int CategoriaID { get; set; }
        [Required(ErrorMessage = "¡Seleccione una categoría!")]
        [JsonIgnore]
        [IgnoreDataMember]
        public TCategoria Categoria { get; set; }
        public ICollection<Inscripcion> Inscripcion { get; set; }
    }
}
