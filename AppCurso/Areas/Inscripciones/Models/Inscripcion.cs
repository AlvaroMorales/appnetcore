﻿using AppCurso.Areas.Cursos.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AppCurso.Areas.Inscripciones.Models
{
    public class Inscripcion
    {
        [Key]
        public int InscripcionId { get; set; }
        public string EstudianteId { get; set; }
        public DateTime Fecha { get; set; }
        public Decimal Pago { get; set; }
        public int CursoId { get; set; }
        public TCursos Curso { get; set; }
    }
}
