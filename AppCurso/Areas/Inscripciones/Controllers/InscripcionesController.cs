﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppCurso.Data;
using AppCurso.Library;
using AppCurso.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace AppCurso.Areas.Inscripciones.Controllers
{
    [Area("Inscripciones")]
    public class InscripcionesController : Controller
    {
        public LCursos curso;
        private SignInManager<IdentityUser> signInManager;
        private UserManager<IdentityUser> userManager;
        private static DataPaginador<DataCurso> models;
        private static IdentityError identityError;
        private ApplicationDbContext context;
        private IWebHostEnvironment hostEnviroment;
        public InscripcionesController(ApplicationDbContext context, UserManager<IdentityUser> userManager
            , SignInManager<IdentityUser> signInManager, IWebHostEnvironment hostEnvironment)
        {
            this.signInManager = signInManager;
            this.userManager = userManager;
            this.hostEnviroment = hostEnvironment;
            curso = new LCursos(context, null);
        }
        public async Task<IActionResult> Inscripciones(int id, string search, int registros)
        {
            if (signInManager.IsSignedIn(User))
            {
                Object[] objects = new Object[3];
                var user = await userManager.GetUserAsync(User);
                var idUser = await userManager.GetUserIdAsync(user);
                var data = curso.Inscripciones(idUser, search);
                if (data.Count > 0)
                {
                    var url = Request.Scheme + "://" + Request.Host.Value;
                    objects = new LPaginador<DataCurso>().paginador(data, id, registros,
                        "Inscripciones", "Inscripciones", "Inscripciones", url);
                }
                else
                {
                    objects[0] = "No hay datos que mostrar";
                    objects[1] = "No hay datos que mostrar";
                    objects[2] = "No hay datos que mostrar";
                }
                models = new DataPaginador<DataCurso>
                {
                    List = (List<DataCurso>)objects[2],
                    Pagi_info = (string)objects[0],
                    Pagi_navegacion = (string)objects[1],
                    Input = new DataCurso()
                };
                return View(models);
            }
            else
            {
                return Redirect("/Home/Index");
            }
        }
        public IActionResult Detalles(int id)
        {
            var model = curso.getTCurso(id);
            return View(model);
        }
        public async Task<IActionResult> Export()
        {
            var list = new List<string[]>();
            if (!models.List.Count.Equals(0))
            {
                foreach (var item in models.List)
                {
                    String[] listData =
                    {
                        item.CursoId.ToString(),
                        item.Curso,
                        item.Informacion,
                        item.Horas.ToString(),
                        String.Format("${0:#,###,###,##0.00####}",item.Costo),
                        item.Estado.ToString(),
                        item.Categoria,
                    };
                    list.Add(listData);
                }
            }
            String[] titles = { "CursoId", "Curso", "Información", "Horas", "Costo", "Estado", "Categoría" };
            var export = new ExportData(hostEnviroment, list, titles, "Inscripciones.xlsx", "Inscripciones");
            return await export.ExportExcelAsync();
        }
    }
}