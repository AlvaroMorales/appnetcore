﻿using AppCurso.Areas.Cursos.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AppCurso.Areas.Categorias.Models
{
    public class TCategoria
    {
        [Key]
        public int CategoriaID { get; set; }
        [Required(ErrorMessage = "Debe ingresar la categoria")]
        public string Categoria { get; set; }
        [Required(ErrorMessage = "Debe ingresar una descrición.")]
        public string Description { get; set; }
        public Boolean Estado { get; set; }
        public ICollection<TCursos> Cursos { get; set; }
    }
}
