﻿using System;
using System.Collections.Generic;
using System.Text;
using AppCurso.Areas.Categorias.Models;
using AppCurso.Areas.Cursos.Models;
using AppCurso.Areas.Inscripciones.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace AppCurso.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<TCategoria> _TCategoria { get; set; }
        public DbSet<TCursos> _TCursos { get; set; }
        public DbSet<Inscripcion> _Inscripcion { get; set; }
    }
}
