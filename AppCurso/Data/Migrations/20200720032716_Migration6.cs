﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AppCurso.Data.Migrations
{
    public partial class Migration6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CategoriaId",
                table: "_Inscripcion");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CategoriaId",
                table: "_Inscripcion",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
