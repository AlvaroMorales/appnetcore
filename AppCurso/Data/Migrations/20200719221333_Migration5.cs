﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AppCurso.Data.Migrations
{
    public partial class Migration5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__Inscripcion__TCursos_CategoriaId",
                table: "_Inscripcion");

            migrationBuilder.DropIndex(
                name: "IX__Inscripcion_CategoriaId",
                table: "_Inscripcion");

            migrationBuilder.AddColumn<int>(
                name: "CursoId",
                table: "_Inscripcion",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX__Inscripcion_CursoId",
                table: "_Inscripcion",
                column: "CursoId");

            migrationBuilder.AddForeignKey(
                name: "FK__Inscripcion__TCursos_CursoId",
                table: "_Inscripcion",
                column: "CursoId",
                principalTable: "_TCursos",
                principalColumn: "CursoId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__Inscripcion__TCursos_CursoId",
                table: "_Inscripcion");

            migrationBuilder.DropIndex(
                name: "IX__Inscripcion_CursoId",
                table: "_Inscripcion");

            migrationBuilder.DropColumn(
                name: "CursoId",
                table: "_Inscripcion");

            migrationBuilder.CreateIndex(
                name: "IX__Inscripcion_CategoriaId",
                table: "_Inscripcion",
                column: "CategoriaId");

            migrationBuilder.AddForeignKey(
                name: "FK__Inscripcion__TCursos_CategoriaId",
                table: "_Inscripcion",
                column: "CategoriaId",
                principalTable: "_TCursos",
                principalColumn: "CursoId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
