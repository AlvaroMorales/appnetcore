﻿using AppCurso.Areas.Cursos.Controllers;
using AppCurso.Areas.Cursos.Models;
using AppCurso.Areas.Inscripciones.Models;
using AppCurso.Data;
using AppCurso.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppCurso.Library
{
    public class LCursos
    {
        private UploadImage _image;
        private ApplicationDbContext context;
        private IWebHostEnvironment environment;

        public LCursos(ApplicationDbContext context, IWebHostEnvironment environment)
        {
            this.context = context;
            this.environment = environment;
            _image = new UploadImage();
        }
        public async Task<IdentityError> RegistrarCursoAsync(DataPaginador<TCursos> model)
        {
            IdentityError identityError;
            try
            {
                if (model.Input.CursoId.Equals(0))
                {
                    var imageByte = await _image.ByteAvatarImageAsync(model.AvatarImage, environment);
                    var curso = new TCursos
                    {
                        Curso = model.Input.Curso,
                        Informacion = model.Input.Informacion,
                        Horas = model.Input.Horas,
                        Costo = model.Input.Costo,
                        Estado = model.Input.Estado,
                        Imagen = imageByte,
                        CategoriaID = model.Input.CategoriaID,
                    };
                    context.Add(curso);
                    context.SaveChanges();
                }
                else
                {
                    byte[] imageByte;
                    if (model.AvatarImage != null)
                    {
                        imageByte = await _image.ByteAvatarImageAsync(model.AvatarImage, environment);
                    }
                    else
                    {
                        imageByte = model.Input.Imagen;
                    }
                    var curso = new TCursos
                    {
                        CursoId = model.Input.CursoId,
                        Curso = model.Input.Curso,
                        Informacion = model.Input.Informacion,
                        Horas = model.Input.Horas,
                        Costo = model.Input.Costo,
                        Estado = model.Input.Estado,
                        Imagen = imageByte,
                        CategoriaID = model.Input.CategoriaID
                    };
                    context.Update(curso);
                    context.SaveChanges();
                }
                identityError = new IdentityError { Code = "Done" };
            }
            catch (Exception ex)
            {
                identityError = new IdentityError { Code = "Error", Description = ex.Message };
            }
            return identityError;
        }

        internal List<TCursos> getTCursos(string search)
        {
            List<TCursos> listCursos;
            if (search == null)
            {
                listCursos = context._TCursos.ToList();
            }
            else
            {
                listCursos = context._TCursos.Where(c => c.Curso.StartsWith(search)).ToList();
            }
            return listCursos;
        }
        internal IdentityError updateEstado(int id)
        {
            IdentityError identityError;
            try
            {
                var curso = context._TCursos.Where(c => c.CursoId.Equals(id)).ToList().ElementAt(0);
                curso.Estado = !curso.Estado;
                context.Update(curso);
                context.SaveChanges();
                identityError = new IdentityError { Description = "Done" };
            }
            catch (Exception ex)
            {
                identityError = new IdentityError { Code = "Error", Description = ex.Message };
            }
            return identityError;
        }
        internal IdentityError DeleteCurso(int cursoId)
        {
            IdentityError identityError;
            try
            {
                var curso = new TCursos
                {
                    CursoId = cursoId
                };
                context.Remove(curso);
                context.SaveChanges();
                identityError = new IdentityError { Description = "Done" };
            }
            catch (Exception e)
            {
                identityError = new IdentityError
                {
                    Code = "Error",
                    Description = e.Message
                };
                throw;
            }
            return identityError;
        }
        public DataCurso getTCurso(int id)
        {
            DataCurso dtCurso = null;
            var query = context._TCategoria.Join(context._TCursos,
                c => c.CategoriaID,
                t => t.CategoriaID,
                (c, t) => new
                {
                    c.CategoriaID,
                    c.Categoria,
                    t.CursoId,
                    t.Curso,
                    t.Informacion,
                    t.Horas,
                    t.Costo,
                    t.Estado,
                    t.Imagen
                }
                ).Where(d => d.CursoId.Equals(id)).ToList();
            if (query.Count > 0)
            {
                var gottenData = query.Last();
                dtCurso = new DataCurso
                {
                    CursoId = gottenData.CursoId,
                    Costo = gottenData.Costo,
                    Estado = gottenData.Estado,
                    Categoria = gottenData.Categoria,
                    Curso = gottenData.Curso,
                    Horas = gottenData.Horas,
                    Imagen = gottenData.Imagen,
                    Informacion = gottenData.Informacion
                };
            }
            return dtCurso;
        }
        public IdentityError Inscripcion(string idUser, int cursoId)
        {
            IdentityError identityError;
            try
            {
                var cursoInscripcion = context._Inscripcion.Where(c => c.CursoId.Equals(cursoId) && c.EstudianteId.Equals(idUser)).ToList();
                if (!(cursoInscripcion.Count > 0))
                {
                    var curso = getTCurso(cursoId);
                    var inscripcion = new Inscripcion
                    {
                        EstudianteId = idUser,
                        Fecha = DateTime.Now,
                        Pago = curso.Costo,
                        CursoId = curso.CursoId
                    };
                    context.Add(inscripcion);
                    context.SaveChanges();
                    identityError = new IdentityError { Description = "Done" };
                }
                else
                {
                    identityError = new IdentityError { Description = "Ya se encuentra suscrito al curso." };
                }
            }
            catch (Exception ex)
            {
                identityError = new IdentityError { Code = "Error", Description = ex.Message };
            }
            return identityError;
        }
        public List<DataCurso> Inscripciones(string idUser, string search)
        {
            List<DataCurso> cursos = new List<DataCurso>();
            var inscripciones = context._Inscripcion.Where(c => c.EstudianteId.Equals(idUser)).ToList();
            if (!inscripciones.Count.Equals(0))
            {
                inscripciones.ForEach(c =>
                {
                    if (search == null || search.Equals(""))
                    {
                        var query = context._TCategoria.Join(context._TCursos,
                            c => c.CategoriaID,
                            t => t.CategoriaID,
                            (c, t) => new
                            {
                                c.CategoriaID,
                                c.Categoria,
                                t.CursoId,
                                t.Informacion,
                                t.Curso,
                                t.Horas,
                                t.Costo,
                                t.Estado,
                                t.Imagen
                            }
                            ).Where(d => d.CursoId.Equals(c.CursoId)).ToList();
                        if (!query.Count.Equals(0))
                        {
                            var data = query.Last();
                            cursos.Add(new DataCurso
                            {
                                CursoId = data.CursoId,
                                Curso = data.Curso,
                                Informacion = data.Informacion,
                                Horas = data.Horas,
                                Costo = data.Costo,
                                Estado = data.Estado,
                                Imagen = data.Imagen,
                                Categoria = data.Categoria
                            });
                        }
                    }
                    else
                    {
                        var query = context._TCategoria.Join(context._TCursos,
                           c => c.CategoriaID,
                           t => t.CategoriaID,
                           (c, t) => new
                           {
                               c.CategoriaID,
                               c.Categoria,
                               t.CursoId,
                               t.Informacion,
                               t.Curso,
                               t.Horas,
                               t.Costo,
                               t.Estado,
                               t.Imagen
                           }
                           ).Where(d => d.CursoId.Equals(c.CursoId) && d.Curso.StartsWith(search)).ToList();
                        if (!query.Count.Equals(0))
                        {
                            var data = query.Last();
                            cursos.Add(new DataCurso
                            {
                                CursoId = data.CursoId,
                                Curso = data.Curso,
                                Informacion = data.Informacion,
                                Horas = data.Horas,
                                Costo = data.Costo,
                                Estado = data.Estado,
                                Imagen = data.Imagen,
                                Categoria = data.Categoria
                            });
                        }
                    }
                });
            }
            return cursos;
        }
    }
}
