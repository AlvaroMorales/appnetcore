﻿using AppCurso.Areas.Categorias.Models;
using AppCurso.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppCurso.Library
{
    public class LCategorias
    {
        private ApplicationDbContext context;

        public LCategorias(ApplicationDbContext context)
        {
            this.context = context;
        }
        public IdentityError RegistrarCategoria(TCategoria categoria)
        {
            IdentityError identityError;
            try
            {
                if (categoria.CategoriaID.Equals(0))
                {
                    context.Add(categoria);
                }
                else
                {
                    context.Update(categoria);
                }
                context.SaveChanges();
                identityError = new IdentityError
                {
                    Code = "Done",
                    Description = "Ok"
                };
            }
            catch (Exception e)
            {
                identityError = new IdentityError
                {
                    Code = "Error",
                    Description = e.Message
                };
            }
            return identityError;
        }
        public List<TCategoria> getTCategoria(string valor)
        {
            List<TCategoria> listCategoria;
            if (valor == null)
            {
                listCategoria = context._TCategoria.ToList();
            }
            else
            {
                listCategoria = context._TCategoria.Where(c => c.Categoria.StartsWith(valor)).ToList();
            }
            return listCategoria;
        }
        internal IdentityError UpdateEstado(int id)
        {
            IdentityError identityError;
            try
            {
                var categoria = context._TCategoria.Where(c => c.CategoriaID.Equals(id)).ToList().ElementAt(0);
                categoria.Estado = !categoria.Estado;
                context.Update(categoria);
                context.SaveChanges();
                identityError = new IdentityError { Description = "Done" };
            }
            catch (Exception e)
            {
                identityError = new IdentityError() { Code = "Error", Description = e.Message };
            }
            return identityError;
        }
        internal IdentityError DeleteCategoria(int categoriaId)
        {
            IdentityError identityError;
            try
            {
                var categoria = new TCategoria { CategoriaID= categoriaId };
                context.Remove(categoria);
                context.SaveChanges();
                identityError = new IdentityError { Description = "Done" };
            }
            catch (Exception ex)
            {
                identityError = new IdentityError
                {
                    Code = "Error",
                    Description = ex.Message
                };
            }
            return identityError;
        }
        public List<SelectListItem> getTCategoria()
        {
            var _selectList = new List<SelectListItem>();
            var categorias = context._TCategoria.Where(c=> c.Estado.Equals(true)).ToList();
            foreach (var categoria in categorias)
            {
                _selectList.Add(new SelectListItem { 
                    Text= categoria.Categoria,
                    Value=categoria.CategoriaID.ToString()
                });
            }
            return _selectList;
        }
    }
}
