﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppCurso.Library
{
    public class LPaginador<T>
    {
        //Cantidad de resultados por página.
        private int pagi_cuantos = 3;
        //Cantidad de enlaces que se mostrarán como máximo en la barra de navegación.
        private int pagi_nav_num_enlaces = 3;
        private int pagi_actual;
        private string pag_nav_anterior = " &laquo; Anterior ";
        private String pag_nav_siguiente = " Siguiente &raquo";
        private String pagi_nav_primera = " laquo; Primero ";
        private String pagi_nav_ultima = " Último &laquo; ";
        private String pagi_navegacion = null;

        public object[] paginador(List<T> table, int pagina, int registros, string area, string controller, string action, string host)
        {
            if (registros > 0)
            {
                pagi_cuantos = registros;
            }
            if (pagina.Equals(0))
            {
                pagi_actual = 1;
            }
            else
            {
                pagi_actual = pagina;
            }
            int pagi_totalReg = table.Count;
            int pagi_totalRegs = pagi_totalReg;
            if ((pagi_totalReg % pagi_cuantos) > 0)
            {
                pagi_totalRegs += 2;
            }
            int pagi_totalPags = pagi_totalRegs / pagi_cuantos;
            if (pagi_actual != 1)
            {
                int pagi_url = 1;
                pagi_navegacion += "<a class='btn btn-default' href='" + host + "/" + controller + "/" + action
                    + "?id=" + pagi_url + "&Registros=" + pagi_cuantos + "&area=" + area + "'>" + pagi_nav_primera + "</a>";

                pagi_url = pagi_actual - 1;
                pagi_navegacion += "<a class='btn btn-default' href='" + host + "/" + controller + "/" + action
                    + "?id=" + pagi_url + "&Registros=" + pagi_cuantos + "&area=" + area + "'>" + pag_nav_anterior + "</a>";
            }
            double valor2 = (pagi_nav_num_enlaces / 2);
            int pagi_nav_intervalo = Convert.ToInt16(Math.Round(valor2));
            int pagi_nav_desde = pagi_actual - pagi_nav_intervalo;
            int pagi_nav_hasta = pagi_actual + pagi_nav_intervalo;
            if (pagi_nav_hasta < 1)
            {
                pagi_nav_hasta += (pagi_nav_desde - 1);
                pagi_nav_desde = 1;
            }
            if (pagi_nav_hasta > pagi_totalPags)
            {
                pagi_nav_desde -= (pagi_nav_hasta - pagi_totalPags);
                pagi_nav_hasta = pagi_totalPags;
                if (pagi_nav_desde < 1)
                {
                    pagi_nav_desde = 1;
                }
            }
            for (int pagi_i = pagi_nav_desde; pagi_i <= pagi_nav_hasta; pagi_i++)
            {
                if (pagi_i == pagi_actual)
                {
                    pagi_navegacion += "<span class='btn btn-default' disabled='disabled'>" + pagi_i + "</span>";
                }
                else
                {
                    pagi_navegacion += "<a class='btn btn-default' href='" + host + "/" + controller + "/" + action
                                        + "?id=" + pagi_i + "&Registros=" + pagi_cuantos + "&area=" + area + "'>" +
                                        pagi_i + "</a>";
                }
            }
            if (pagi_actual < pagi_totalPags)
            {
                int pagi_url = pagi_actual + 1;     
                pagi_navegacion += "<a class='btn btn-default' href='" + host + "/" + controller + "/" + action
                       + "?id=" + pagi_url + "&Registros=" + pagi_cuantos + "&area=" + area + "'>" + pag_nav_siguiente + "</a>";

                pagi_url = pagi_totalPags;
                pagi_navegacion += "<a class='btn btn-default' href='" + host + "/" + controller + "/" + action
               + "?id=" + pagi_url + "&Registros=" + pagi_cuantos + "&area=" + area + "'>"
               + pagi_nav_ultima + "</a>";
            }
            int pagi_inicial = (pagi_actual - 1) * pagi_cuantos;
            var query = table.Skip(pagi_inicial).Take(pagi_cuantos).ToList();

            string pagi_info = " del <b>" + pagi_actual + "</b> al <b>" + pagi_totalPags + "</b> de <b>" + pagi_totalReg +
                "</b> <b>/" + pagi_cuantos + " registos por página </b>";
            object[] data = { pagi_info, pagi_navegacion, query };
            return data;
        }
    }
}
