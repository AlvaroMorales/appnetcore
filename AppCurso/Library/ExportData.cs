﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace AppCurso.Library
{
    public class ExportData : Controller
    {
        private List<String[]> listData;
        private string fileName, table;
        private String[] titles;
        private IWebHostEnvironment hostEnvironment;

        public ExportData(IWebHostEnvironment hostEnvironment,List<String[]> listData,String[] titles,
        string fileName,string table)
        {
            this.hostEnvironment = hostEnvironment;
            this.listData = listData;
            this.titles = titles;
            this.fileName = fileName;
            this.table = table;
        }
        public async Task<IActionResult> ExportExcelAsync()
        {
            string sWebRoot = hostEnvironment.WebRootPath;
            var memory = new MemoryStream();
            using (var fs= new FileStream(Path.Combine(sWebRoot, fileName), FileMode.Create, FileAccess.Write))
            {
                IWorkbook workbook;
                workbook = new XSSFWorkbook();
                ISheet sheet = workbook.CreateSheet(table);
                IRow row = sheet.CreateRow(0);
                for (int i = 0; i < titles.Length; i++)
                {
                    row.CreateCell(i).SetCellValue(titles[i]);
                }
                int count = 1;
                for (int i = 0; i < listData.Count; i++)
                {
                    row = sheet.CreateRow(count);
                    var list = listData[i];
                    for (int j = 0; j < titles.Length; j++)
                    {
                        row.CreateCell(j).SetCellValue(list[j]);
                    }
                    count++;
                }
                workbook.Write(fs);
            }
            using (var stream= new FileStream(Path.Combine(sWebRoot,fileName),FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory,
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",fileName);
        }
    }
}
