﻿using AppCurso.Areas.Cursos.Controllers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AppCurso.Library
{
    public class UploadImage
    {
        public async Task<byte[]> ByteAvatarImageAsync(IFormFile AvatarImage, IWebHostEnvironment environment) 
        {
            if (AvatarImage != null)
            {
                using (var memoryStream=new MemoryStream())
                {
                    await AvatarImage.CopyToAsync(memoryStream);
                    return memoryStream.ToArray();
                }
            }
            else
            {
                var archivoOrigen = environment.ContentRootPath + "/wwwroot/Images/Logo2.png";
                return File.ReadAllBytes(archivoOrigen);
            }
        }
    }
}
