﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using AppCurso.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Identity;
using AppCurso.Library;
using AppCurso.Data;
using AppCurso.Areas.Cursos.Models;

namespace AppCurso.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private LCursos curso;
        private static DataPaginador<TCursos> models;
        private static DataCurso dtCurso;
        private SignInManager<IdentityUser> signInManager;
        private UserManager<IdentityUser> userManager;
        private static IdentityError identityError;

        public HomeController(ApplicationDbContext context,ILogger<HomeController> logger,
            UserManager<IdentityUser> userManager,SignInManager<IdentityUser> signInManager)
        {
            _logger = logger;
            this.userManager = userManager;
            this.signInManager = signInManager;
            curso = new LCursos(context,null);
        }

        public IActionResult Index(int id,String filtrar)
        {
            Object[] objects = new object[3];
            var data = curso.getTCursos(filtrar);
            if (data.Count>0)
            {
                var url = Request.Scheme + "://" + Request.Host.Value;
                objects = new LPaginador<TCursos>().paginador(data,id,10,"","Home","Index",url);
            }
            else
            {
                objects[0] = "No hay datos que mostrar";
                objects[1] = "No hay datos que mostrar";
                objects[2] = new List<TCursos>();
            }
            models = new DataPaginador<TCursos>
            {
                List = (List<TCursos>)objects[2],
                Pagi_info = (String)objects[0],
                Pagi_navegacion = (String)objects[1],
                Input = new TCursos() 
            };
            if (identityError!=null)
            {
                models.Pagi_info = identityError.Description;
                identityError = null;
            }
            //await CreateRolesAsync(_serviceProvider);
            return View(models);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        private async Task CreateRolesAsync(IServiceProvider service)
        {
            var roleManager = service.GetRequiredService<RoleManager<IdentityRole>>();
            var userManager = service.GetRequiredService<UserManager<IdentityUser>>();
            String[] rolesName = { "Admin", "Student" };
            foreach (var item in rolesName)
            {
                var roleExist = await roleManager.RoleExistsAsync(item);
                if (!roleExist)
                {
                    await roleManager.CreateAsync(new IdentityRole(item));
                }
            }
            var user = await userManager.FindByIdAsync("7f375673-1627-4af7-9bcf-988b8b8e681d");
            await userManager.AddToRoleAsync(user,"Admin");
        }
        public IActionResult Detalles(int id)
        {
            var model = curso.getTCurso(id);
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Obtener(int cursoId,int vista)
        {
            if (signInManager.IsSignedIn(User))
            {
                var user = await userManager.GetUserAsync(User);
                var idUser = await userManager.GetUserIdAsync(user);
                var data = curso.Inscripcion(idUser,cursoId);
                if (data.Description.Equals("Done"))
                {
                    return Redirect("/Inscripciones/Inscripciones?area=Inscripciones");
                }
                else
                {
                    identityError = data;
                    if (vista.Equals(1))
                    {
                        return Redirect("/Home/Index");
                    }
                    else
                    {
                        dtCurso = curso.getTCurso(cursoId);
                        dtCurso.ErrorMessage = data.Description;
                        return View("Detalles",dtCurso);
                    }
                }
            }
            else
            {
                return Redirect("/Identity/Account/Login");
            }
        }
        
    }
}
