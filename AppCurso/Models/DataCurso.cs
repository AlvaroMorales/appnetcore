﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppCurso.Models
{
    public class DataCurso
    {
        public int CursoId { get; set; }
        public string Curso { get; set; }
        public string Informacion { get; set; }
        public byte Horas { get; set; }
        public decimal Costo { get; set; }
        public Boolean Estado { get; set; }
        public byte[] Imagen { get; set; }
        public string Categoria { get; set; }
        public string ErrorMessage { get; set; }
    }
}
