﻿
class Categorias {
    constructor() {
        this.CategoriaId = 0;
    }
    RegistrarCategoria() {
        $.post(
            "GetCategoria",
            $('.formCategoria').serialize(),
            (response) => {
                try {
                    var item = JSON.parse(response);
                    if (item.Code == "Done") {
                        window.location.href == "Categoria";
                    } else {
                        document.getElementById("mensaje").innerHTML = item.Description;
                    }
                    console.log(response);
                } catch (e) {
                    document.getElementById("mensaje").innerHTML = response;
                }
            }
        );
    }
    EditCategoria(data) {
        document.getElementById("catNombre").value = data.Nombre;
        document.getElementById("catDescripcion").value = data.Description;
        document.getElementById("catEstado").value = data.Estado;
        document.getElementById("catCategoryId").value = data.CategoriaID;
        console.log(data);
    }
    GetCategoria(data) {
        document.getElementById("titleCategoria").innerHTML = data.Nombre;
        this.CategoriaId = data.CategoriaID;
    }
    EliminarCategoria() {
        $.post(
            "EliminarCategoria",
            { CategoriaID: this.CategoriaId },
            (response) => {
                var item = JSON.parse(response);
                if (item.Description == "Done") {
                    window.location.href = "Categoria";
                }
                else {
                    document.getElementById("mensajeEliminar").innerHTML = item.Description;
                }
            }
        );
    }
    Restablecer() {
        document.getElementById("catNombre").value = "";
        document.getElementById("catDescripcion").value = "";
        document.getElementById("catEstado").value = false;
        document.getElementById("catCategoryId").value = 0;
    }

}
