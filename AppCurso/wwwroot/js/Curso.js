﻿
class Cursos extends UploadPicture {
    constructor() {
        super();
        this.Image = null;
        this.CursoId = 0;
    }
    RegistrarCurso() {
        var data = new FormData();
        data.append('Input.Curso', $("#curNombre").val());
        data.append('Input.Informacion', $("#curDescription").val());
        data.append('Input.Horas', $("#curHoras").val());
        data.append('Input.Costo', $("#curCosto").val());
        data.append('Input.Estado', document.getElementById("curEstado").checked);
        data.append('Input.CategoriaID', $("#curCategoria").val());
        data.append('Input.Imagen', this.Image);
        data.append('Input.CursoId', $("#curCursoId").val());
        $.each($('input[type=file]')[0].files, (i, file) => {
            data.append('AvatarImage', file);
        });
        $.ajax({
            url: "GetCurso",
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'Post',
            success: (result) => {
                try {
                    var item = JSON.parse(result);
                    if (item.Code = "Done") {
                        window.location.href = "Cursos";
                    }
                    else {
                        document.getElementById("mensaje").innerHTML = item.Description;
                    }
                } catch (e) {
                    document.getElementById("mensaje").innerHTML = result;
                }
                console.log(result);
            }
        });
    }
    EditCurso(curso, cat) {
        let j = 1;
        $("#curNombre").val(curso.Nombre);
        $("#curDescription").val(curso.Descripcion);
        $("#curHoras").val(curso.Horas);
        $("#curCosto").val(curso.Costo);
        $("#curEstado").prop("checked", curso.Estado);
        $("#curCursoId").val(curso.CursoId);
        this.Image = curso.Imagen;
        document.getElementById("cursoImage").innerHTML = "<img class='img-fluid img-thumbnail rounded-circle' src='data:image/jpeg;base64," + curso.Imagen + "' />";
        let x = document.getElementById('curCategoria');
        x.options.length = 0;
        for (var i = 0; i < cat.length; i++) {
            x.options[i] = new Option(cat[i].Text, cat[i].Value);

            if (cat[i].Value == curso.CategoriaID) {
                j = i;
            }
        }
        x.options[0] = new Option(cat[j].Text, cat[j].Value);
        x.options[j] = new Option(cat[0].Text, cat[0].Value);
        x.selectedIndex = 0;
        // x.remove(j);
        console.log(curso);
        console.log(cat);
    }
    GetCurso(curso) {
        this.Image = curso.Imagen;
        document.getElementById("cursoImage").innerHTML = "<img class='img-fluid img-thumbnail rounded-circle' src='data:image/jpeg;base64," + curso.Imagen + "' />";
        document.getElementById("titleCurso").innerHTML = curso.Nombre;
        this.CursoId = curso.CursoId;

        console.log(curso);
    }
    EliminarCurso() {
        document.getElementById("cursoImage").innerHTML = "<img class='img-fluid img-thumbnail rounded-circle' src='data:image/jpeg;base64," + this.Image + "' />";
        $.post(
            "EliminarCurso",
            { CursoId: this.CursoId },
            (response) => {
                var item = JSON.parse(response);
                if (item.Description == "Done") {
                    window.location.href == "Cursos";
                }
                else {
                    document.getElementById("mensajeEliminar").innerHTML = item.Descripcion;
                }
            }
        );
    }
    Restablecer() {
        $("#curNombre").val("");
        $("#curDescription").val("");
        $("#curHoras").val("");
        $("#curCosto").val("");
        $("#curEstado").prop("checked", false);
        $("#curCursoId").val("");
        document.getElementById("cursoImage").innerHTML = ['<img class="img-fluid img-thumbnail rounded-circle" src="/images/Logo2.png"/>'].join('');
    }
}   
